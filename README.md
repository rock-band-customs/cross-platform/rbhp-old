# The Rock Band Harmonies Project

This repo contains a very old copy of RBHP kept by Koetsu on ScoreHero. Here, it has been organized and somewhat cleaned up for convenience.

If using Git, simply checkout one of the branches below and copy to another folder, else download the one(s) you want via ZIP, TAR, or whatever. Place the files in folders explained in the wiki and pack.

| **Title** | **Branch(es)** |
|:-----:|:-----:|
| **Rock Band:** | `RB1_Disc` |
| **Rock Band 2** | `RB2_Disc`, `RB2_20_Free` |
| **LEGO Rock Band** | `LRB_Disc` |
| **AC/DC Live Track Pack** | `ACDC_Live_TP` |
| **Rock Band DLC** | `DLC_2007`, `DLC_2008`, `DLC_2009`, `DLC_2010`,`RBN_DLC` |
| **Rock Band Pro Drums** | `RBN_Pro_Drums` |
| **Optional Upgrades** | `RB3_Upgrades` |

For a more complete, up-to-date list, you may want to try the [Rock Band Harmonies Project on Github](https://github.com/FujiSkunk/rbhp). **FujiSkunk**, who also is on Rhythm Gaming World and Rock Band Customs Discord, is doing his best to keep it up to date.
